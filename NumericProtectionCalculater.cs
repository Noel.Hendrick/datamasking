﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using OfficeOpenXml;
using System.Linq;

namespace DataMasking
{
    public class NumericProtectionCalculater
    {
        public static Calculator calculator = new Calculator();
        ExcelHandler excelHandler = new ExcelHandler();


        public Tuple<double, double, double> CalculateQualityOfData(double[,] Dataset)
        {
            // this gets the orginal Dataset, calls the disclosure risk and information loss methods, then returns a Tuple with both doubles inside
            double[,] OrginalDataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\StandardData.xlsx", "Census", 2, 1);
            double DisclosureRisk = (CalculateDisclosureRisk(Dataset, OrginalDataset) / 100);
            double InformationLoss = CalculateInformationLoss(Dataset, OrginalDataset);
            double result = (DisclosureRisk + InformationLoss) / 2;
            return Tuple.Create<double, double, double>(calculator.SetRoundedValue(result), DisclosureRisk, InformationLoss);


        }


        public static double CalculateDisclosureRisk(double[,] Dataset, double[,] OrginalDataset)
        {

            // takes in the new dataset and the orginal dataset, it measusres the disclosure risk using the Distance based disclosure risk formula 
            double ShortestDistance = 1000000000000000000;
            int ShortestDistanceRowNumber = 0;
            int NumberOfDiscoveredRows = 0;
            double NumberOfRows = 0;
            for (int t = 0; t < Dataset.GetLength(0); t++)
            {

                List<double> OrginalRow = calculator.ConvertRowToList(Dataset, t);
                ShortestDistance = 1000000000000000000;

                for (int i = 0; i < Dataset.GetLength(0); i++)
                {

                    List<double> ProtectedRow = calculator.ConvertRowToList(OrginalDataset, i);
                    double CurrentDistance = calculator.CalculateEuclideanDistance(OrginalRow, ProtectedRow);

                    if (CurrentDistance < ShortestDistance)
                    {
                        ShortestDistance = CurrentDistance;
                        ShortestDistanceRowNumber = i;
                    }


                }
                if (ShortestDistanceRowNumber == t)
                {
                    NumberOfDiscoveredRows++;
                }

                NumberOfRows++;

            }


            double disclosureRisk = (((double)NumberOfDiscoveredRows / NumberOfRows) * 100);

            disclosureRisk = calculator.SetRoundedValue(disclosureRisk);

            return disclosureRisk;

        }


        public double CalculateInformationLoss(double[,] DataSet, double[,] OrginalDataset)
        {
            // this method calculates a corrilation matrix for each of the two datasets, it then passes both of these datasets into the mean square error method 
            double[,] CorrilationMatrixOfProtectedData = CalculateCorrilationMatrix(DataSet);
            double[,] CorrilationMatrixOfOrginalData = CalculateCorrilationMatrix(OrginalDataset);
            return calculator.CalculateMeanSquareError(CorrilationMatrixOfProtectedData, CorrilationMatrixOfOrginalData);
        }


        public static double[,] CalculateCorrilationMatrix(double[,] Dataset)
        {
            VariableNameToAnalyticsDictionary ColumnDictionary = new VariableNameToAnalyticsDictionary();
            double[,] CorrilationMatrix = new double[Dataset.GetLength(1), Dataset.GetLength(1)];

            // this starts by making a dictionary, each column gets an entry and with its mean as the value  
            for (int i = 0; i < Dataset.GetLength(1); i++)
            {

                List<double> DataList = calculator.ConvertColumnToList(Dataset, i);

                VaribleAnalytics varibleAnalytics = new VaribleAnalytics();
                varibleAnalytics.Mean = calculator.CalculateMean(DataList);
                ColumnDictionary.AddToDictionary("Column" + i, varibleAnalytics);

            }

            //using the dictionary, this loop claculates the corrilation between each column against every other column and puts it into a 2D array 
            int XIndexOfArray = 0;
            int YIndexOfArray = 0;
            for (int i = 0; i < Dataset.GetLength(1); i++)
            {
                List<double> DataListX = calculator.ConvertColumnToList(Dataset, i);
                string XKey = "Column" + i;
                YIndexOfArray = 0;

                for (int j = 0; j < Dataset.GetLength(1); j++)
                {
                    List<double> DataListY = calculator.ConvertColumnToList(Dataset, j);
                    string YKey = "Column" + j;

                    double Corrilation = FindCorrilation(DataListX, XKey, DataListY, YKey, ColumnDictionary);
                    CorrilationMatrix[XIndexOfArray, YIndexOfArray] = Corrilation;
                    YIndexOfArray++;
                }
                XIndexOfArray++;
            }
            return CorrilationMatrix;

        }


        public static double FindCorrilation(List<double> DataListX, string XKey, List<double> DataListY, string YKey, VariableNameToAnalyticsDictionary ColumnDictionary)
        {
            // this method calculates the corrilation between two different datasets using the dictionary to find the mean and save runtime
            var Mean1 = ColumnDictionary.GetValueForKey(XKey).Mean;
            var Mean2 = ColumnDictionary.GetValueForKey(YKey).Mean;

            var sum1 = DataListX.Zip(DataListY, (x1, y1) => (x1 - Mean1) * (y1 - Mean2)).Sum();

            var sumSqr1 = DataListX.Sum(x => Math.Pow((x - Mean1), 2.0));
            var sumSqr2 = DataListY.Sum(y => Math.Pow((y - Mean2), 2.0));

            if (sumSqr1 == 0)
            {
                sumSqr1 = 0.00000000000001;
            }

            if (sumSqr2 == 0)
            {
                sumSqr2 = 0.00000000000001;
            }

            var result = sum1 / Math.Sqrt(sumSqr1 * sumSqr2);

            return result;
        }

    }
}
