﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using OfficeOpenXml;

namespace DataMasking
{
    public class ExcelHandler
    {
        ExcelPackage package;
        ExcelWorksheet Dataset;
        int StartingRowNumber = 0;
        int StartingColumnNumber = 0;

        public double[,] GetExcelSheet(string filePath, string WorksheetName, int _startingRowNumber, int _startingColumnNumber)
        {
            //this method takes in a file path, worksheet name and the starting values of the numerical data, it then gets the excel sheet, converts the data into a 2D Array and returns it 
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            FileInfo fileInfo = new FileInfo(filePath);
            package = new ExcelPackage(fileInfo);
            Dataset = package.Workbook.Worksheets[WorksheetName];
            StartingRowNumber = _startingRowNumber;
            StartingColumnNumber = _startingColumnNumber;
            int RowIndex = (Dataset.Dimension.Rows - (StartingRowNumber - 1));
            int ColumnIndex = (Dataset.Dimension.Columns - (StartingColumnNumber - 1));
            double[,] DatasetArray = new double[RowIndex, ColumnIndex];

            String StringValue = "";
            for (int i = 0; i < RowIndex; i++)
            {
                _startingColumnNumber = StartingColumnNumber;
                for (int j = 0; j < ColumnIndex; j++)
                {

                    StringValue = Dataset.Cells[_startingRowNumber, _startingColumnNumber].Value.ToString();
                    DatasetArray[i, j] = (double.Parse(StringValue));
                    _startingColumnNumber++;
                }
                _startingRowNumber++;
            }
            return DatasetArray;

        }


        public string[,] GetCategoricalExcelSheet(string filePath, string WorksheetName, int _startingRowNumber, int _startingColumnNumber)
        {
            //this method takes in a file path, worksheet name and the starting values of the Categorical data, it then gets the excel sheet, converts the data into a 2D Array and returns it 
            FileInfo fileInfo = new FileInfo(filePath);
            package = new ExcelPackage(fileInfo);
            Dataset = package.Workbook.Worksheets[WorksheetName];
            StartingRowNumber = _startingRowNumber;
            StartingColumnNumber = _startingColumnNumber;
            int RowIndex = (Dataset.Dimension.Rows - (StartingRowNumber - 1));
            int ColumnIndex = (Dataset.Dimension.Columns - (StartingColumnNumber - 1));
            string[,] DatasetArray = new string[RowIndex, ColumnIndex];

            for (int i = 0; i < RowIndex; i++)
            {
                _startingColumnNumber = StartingColumnNumber;
                for (int j = 0; j < ColumnIndex; j++)
                {
                    DatasetArray[i, j] = Dataset.Cells[_startingRowNumber, _startingColumnNumber].Value.ToString();
                    _startingColumnNumber++;
                }
                _startingRowNumber++;
            }
            return DatasetArray;

        }


        public void SaveAsNewFile(double[,] DatasetArray, string filePath)
        {
            // this method handles the saving of the worksheet by converting the 2D array back into a worksheet and saving it
            FileInfo Filepath = new FileInfo(filePath);
            int _startingColumnNumber;
            int _startingRowNumber = StartingRowNumber;
            for (int i = 0; i < DatasetArray.GetLength(0); i++)
            {
                _startingColumnNumber = StartingColumnNumber;
                for (int j = 0; j < DatasetArray.GetLength(1); j++)
                {

                    Dataset.Cells[_startingRowNumber, _startingColumnNumber].Value = DatasetArray[i, j];

                    _startingColumnNumber++;
                }
                _startingRowNumber++;
            }



            package.SaveAs(Filepath);
        }


        public void SaveAsNewFile(string[,] DatasetArray, string filePath)
        {
            // this method handles the saving of the worksheet by converting the 2D array back into a worksheet and saving it
            FileInfo Filepath = new FileInfo(filePath);
            int _startingColumnNumber;
            int _startingRowNumber = StartingRowNumber;
            for (int i = 0; i < DatasetArray.GetLength(0); i++)
            {
                _startingColumnNumber = StartingColumnNumber;
                for (int j = 0; j < DatasetArray.GetLength(1); j++)
                {

                    Dataset.Cells[_startingRowNumber, _startingColumnNumber].Value = DatasetArray[i, j];

                    _startingColumnNumber++;
                }
                _startingRowNumber++;
            }



            package.SaveAs(Filepath);
        }
    }
}