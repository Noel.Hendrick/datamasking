﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataMasking
{
    // This is a custom dictionary class using the column name as a key and the VaribleAnalytics object as a value
    public class VariableNameToAnalyticsDictionary
    {
        private Dictionary<string, VaribleAnalytics> _dictionary = new Dictionary<string, VaribleAnalytics>();

        public bool AddToDictionary(string key, VaribleAnalytics value)
        {
            if (!_dictionary.ContainsKey(key))
            {
                _dictionary.Add(key, value);
                return true;
            }

            return false;
        }


        public VaribleAnalytics GetValueForKey(string key)
        {
            VaribleAnalytics varOut;
            _dictionary.TryGetValue(key, out varOut);

            return varOut;
        }
    }
}
