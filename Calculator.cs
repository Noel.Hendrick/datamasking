﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OfficeOpenXml;

namespace DataMasking
{
    public class Calculator
    {
        public double CalculateMean(IList<double> Dataset)
        {
            // gets the average of the dataset 
            double Mean = Dataset.Average();
            Mean = SetRoundedValue(Mean);
            return Mean;

        }


        public double CalculateStandardDeviation(IList<double> Dataset)
        {

            // gets the standard deviation of a dataset
            double ret = 0;
            int count = Dataset.Count();
            if (count > 1)
            {

                double Mean = CalculateMean(Dataset);

                //Perform the Sum of (value-avg)^2
                double sumOfDataset = Dataset.Sum(d => (d - Mean) * (d - Mean));


                ret = Math.Sqrt(sumOfDataset / count);
            }


            ret = SetRoundedValue(ret);
            return ret;


        }


        public double CalculateStandardDeviationGivenMean(IList<double> Dataset, double Mean)
        {

            // gets the standard deviation of a dataset with a given Mean
            double ret = 0;
            int count = Dataset.Count();
            if (count > 1)
            {

                //Perform the Sum of (value-avg)^2
                double sumOfDataset = Dataset.Sum(d => (d - Mean) * (d - Mean));


                ret = Math.Sqrt(sumOfDataset / count);
            }


            ret = SetRoundedValue(ret);
            return ret;


        }


        public Tuple<double, double> ReturnStandardDeviationAndMean(IList<double> Dataset)
        {

            // returns both the standard deviation and mean of the dataset in a tuple
            double StandardDeviation = 0;
            double Mean = 0;
            int count = Dataset.Count();
            if (count > 1)
            {

                Mean = CalculateMean(Dataset);

                //Perform the Sum of (value-avg)^2
                double sumOfDataset = Dataset.Sum(d => (d - Mean) * (d - Mean));

                StandardDeviation = Math.Sqrt(sumOfDataset / count);
            }


            StandardDeviation = SetRoundedValue(StandardDeviation);
            return Tuple.Create<double, double>(Mean, StandardDeviation);


        }

        public List<double> ConvertToList(double[,] Dataset)
        {
            // converts a dataset to a list
            List<double> DataList = new List<double>();
            for (int RowIndex = 0; RowIndex < Dataset.GetLength(0); RowIndex++)
            {
                for (int ColumnIndex = 0; ColumnIndex < Dataset.GetLength(1); ColumnIndex++)
                {
                    DataList.Add(Dataset[RowIndex, ColumnIndex]);
                }
            }

            return DataList;
        }

        public List<double> ConvertColumnToList(double[,] Dataset, int ColumnIndex)
        {
            // converts a column in  a dataset to a list
            List<double> DataList = new List<double>();
            for (int RowIndex = 0; RowIndex < Dataset.GetLength(0); RowIndex++)
            {

                DataList.Add(Dataset[RowIndex, ColumnIndex]);



            }

            return DataList;
        }

        public List<string> ConvertColumnToList(string[,] Dataset, int ColumnIndex)
        {
            // converts a column in  a dataset to a list
            List<string> DataList = new List<string>();
            for (int RowIndex = 0; RowIndex < Dataset.GetLength(0); RowIndex++)
            {

                DataList.Add(Dataset[RowIndex, ColumnIndex]);



            }

            return DataList;
        }



        public List<double> ConvertRowToList(double[,] Dataset, int RowIndex)
        {
            //converts a row in a dataset to a list
            List<double> DataList = new List<double>();
            for (int ColumnIndex = 0; ColumnIndex < Dataset.GetLength(1); ColumnIndex++)
            {

                DataList.Add(Dataset[RowIndex, ColumnIndex]);



            }

            return DataList;
        }



        public List<string> ConvertRowToListCategoricalData(string[,] Dataset, int RowIndex)
        {
            //converts a row in a dataset to a list
            List<string> DataList = new List<string>();
            for (int ColumnIndex = 0; ColumnIndex < Dataset.GetLength(1); ColumnIndex++)
            {

                DataList.Add(Dataset[RowIndex, ColumnIndex]);



            }

            return DataList;
        }



        public double CalculateVarience(IList<double> Dataset)
        {

            // gets the varience of a dataset
            double ret = 0;
            int count = Dataset.Count();
            if (count > 1)
            {

                double Mean = CalculateMean(Dataset);

                //Perform the Sum of (value-avg)^2
                double sumOfDataset = Dataset.Sum(d => (d - Mean) * (d - Mean));


                ret = sumOfDataset / count;
            }

            ret = SetRoundedValue(ret);
            return ret;


        }


        public Tuple<double, double, double> ReturnStandardDeviationAndMeanAndVarience(IList<double> Dataset)
        {
            // returns a tuple of mean, standard deviation and varience
            double Varience = 0;
            double StandardDeviation = 0;
            double Mean = 0;
            int count = Dataset.Count();
            if (count > 1)
            {

                Mean = CalculateMean(Dataset);

                //Perform the Sum of (value-avg)^2
                double sumOfDataset = Dataset.Sum(d => (d - Mean) * (d - Mean));

                Varience = sumOfDataset / count;

                StandardDeviation = Math.Sqrt(Varience);
            }


            StandardDeviation = SetRoundedValue(StandardDeviation);
            Varience = SetRoundedValue(Varience);
            return Tuple.Create<double, double, double>(Mean, StandardDeviation, Varience);


        }


        public double SetRoundedValue(double UnroundedValue)
        {
            // rounds any double its given to 16 decimal places
            UnroundedValue = Math.Round(UnroundedValue, 14);
            return UnroundedValue;
        }


        public double CalculateEuclideanDistance(IList<double> Dataset1, IList<double> Dataset2)
        {

            // calculates the distance between two datasets
            double Sum = 0;
            double distance = 0;
            for (int i = 0; i < Dataset1.Count; i++)
            {
                Sum = Sum + Math.Pow((Dataset1[i] - Dataset2[i]), 2.0);

            }

            distance = Math.Sqrt(Sum);
            distance = SetRoundedValue(distance);
            return distance;


        }

        public double CalculateMeanSquareError(double[,] Matrix1, double[,] Matrix2)
        {
            //calculates the mean square error between two matrices
            double NumberOfEntries = 0;
            double Sum = 0;
            double MeanSquareError = 0;
            for (int i = 0; i < Matrix1.GetLength(0); i++)
            {
                for (int j = 0; j < Matrix1.GetLength(1); j++)
                {
                    Sum = Sum + Math.Pow((Matrix1[i, j] - Matrix2[i, j]), 2.0);
                    NumberOfEntries++;
                }
            }

            MeanSquareError = Sum / NumberOfEntries;
            MeanSquareError = SetRoundedValue(MeanSquareError);
            return MeanSquareError;

        }

        public int FindCommonElements(List<string> OrginalRow, List<string> ProtectedRow)
        {
            int CommomCounter = 0;
            for (int i = 0; i < OrginalRow.Count; i++)
            {
                if (OrginalRow[i] == ProtectedRow[i])
                {
                    CommomCounter++;
                }
            }

            return CommomCounter;
        }



    }
}

