﻿using System;

namespace DataMasking
{
    class Program
    {
        public static string UseStatistic;

        public static void Main()
        {
            ExcelHandler excelHandler = new ExcelHandler();

            //Retrives the Orginal Excelsheet
            double[,] Dataset = excelHandler.GetExcelSheet(@"C:\Users\USER\Downloads\TestExcelsheets\CASCrefmicrodata.xlsx", "Census", 2, 1);

            //Calls the data standizer method and saves the result in a new file
            Dataset = GetNormalisedData(Dataset);
            excelHandler.SaveAsNewFile(Dataset, @"C:\Users\USER\Downloads\TestExcelsheets\StandardData.xlsx");

            // Calls the Protection method and saves the result in a new file
            Dataset = NumericProtectData(Dataset);
            excelHandler.SaveAsNewFile(Dataset, @"C:\Users\USER\Downloads\TestExcelsheets\DataWithNoise.xlsx");

            //Get protected stats
            NumericProtectionCalculater calc = new NumericProtectionCalculater();
            var result = calc.CalculateQualityOfData(Dataset);
            Console.WriteLine("Disclosure Risk: "+result.Item2);
            Console.WriteLine("Information Loss: "+result.Item3);

        }

        public static double[,] GetNormalisedData(double[,] Dataset)
        {
            //Creates an instance of the DataStandardizer and call the Data Standardizing method and then return the changed Data
            DataStandardizer DataChanger = new DataStandardizer();
            Dataset = DataChanger.DataStandardizing(Dataset);
            return Dataset;


        }

        public static double[,] NumericProtectData(double[,] Dataset)
        { 
            RankSwapping rank = new RankSwapping();
            //Dataset = NoiseAddition.AddNoiseAdditon(Dataset, 50);

            Dataset = rank.RankSwapper(Dataset, 24);
            return Dataset;

        }

    }
}